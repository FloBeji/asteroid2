﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

    public GameObject Laser;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        MoveShip();

        if (Input.GetKeyDown(KeyCode.Space))
            Instantiate(Laser);
    }

    private void MoveShip()
    {
        float xModif = 0;
        float yModif = 0;
        float incrementValue = 1.0f;
        float xBorder = 8.0f;
        float yUpBorder = 13.0f;
        float yDownBorder = 0.0f;

        if (Input.GetKeyDown(KeyCode.RightArrow)) xModif += incrementValue;
        if (Input.GetKeyDown(KeyCode.LeftArrow)) xModif -= incrementValue;
        if (Input.GetKeyDown(KeyCode.UpArrow)) yModif += incrementValue;
        if (Input.GetKeyDown(KeyCode.DownArrow)) yModif -= incrementValue;
        
        if (xModif == 0 && yModif == 0)
        {
            return;
        }

        Vector3 currentPosition = gameObject.transform.position;
        if (currentPosition.x + xModif > xBorder) xModif = -xBorder * 2;
        if (currentPosition.x + xModif < -1 * xBorder) xModif = xBorder * 2;
        if (currentPosition.y + yModif > yUpBorder) yModif = -12;
        if (currentPosition.y + yModif < yDownBorder) yModif = 12;

        gameObject.transform.Translate(0, yModif, xModif);
    }
}
