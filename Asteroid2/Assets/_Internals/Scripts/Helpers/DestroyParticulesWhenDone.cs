﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticulesWhenDone : MonoBehaviour {

    public bool IsConcerningParent = false;

	void Update ()
    {
        if (!GetComponent<ParticleSystem>().isPlaying)
            if (IsConcerningParent)
                Destroy(gameObject.transform.parent.gameObject);
            else
                Destroy(gameObject);
    }
}
