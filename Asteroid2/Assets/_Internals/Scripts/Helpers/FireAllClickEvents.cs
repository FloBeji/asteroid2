﻿//Copyright 2012 Maximilian Wolfgang Maroe, feel free to use or modify this script at your leisure
//This script sets it up so that when you click or touch, it tries to find an object in the scene
//that is in the direction of where you clicked, and sends a "Clicked()" message to the object.
//Supports touch (And multitouch!)

using UnityEngine;
using System.Collections;

public class FireAllClickEvents : MonoBehaviour
{
    const float TIME_LONG_TOUCH = 1f;

    private Camera _Camera;
    public bool Debug;

    void Start()
    {
        _Camera = Camera.main;
    }
    public float TouchTime;

    void Update()
    {
        Ray ray;
        RaycastHit hit;

        if (Application.platform == RuntimePlatform.IPhonePlayer || 
            Application.platform == RuntimePlatform.Android)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    ray = _Camera.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                    {
                        TouchTime = Time.time;
                        if (Debug) UnityEngine.Debug.Log("You touched " + hit.collider.gameObject.name, hit.collider.gameObject);
                        hit.transform.gameObject.SendMessage("Clicked", hit.point, SendMessageOptions.DontRequireReceiver);
                    }
                }

                if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {
                    ray = _Camera.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                    {
                        if (Time.time - TouchTime >= TIME_LONG_TOUCH)
                            hit.transform.gameObject.SendMessage("LongTouched", hit.point, SendMessageOptions.DontRequireReceiver);
                    }
                }
            }
        }
        else //We are on a computer (more than likely)
        {

            if (Input.GetMouseButtonDown(0))  //Left Click
            {
                ray = _Camera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (Debug) UnityEngine.Debug.Log("You clicked " + hit.collider.gameObject.name, hit.collider.gameObject);

                    hit.transform.gameObject.SendMessage("Clicked", hit.point, SendMessageOptions.DontRequireReceiver);
                }
            }

            if (Input.GetMouseButtonDown(1))  //Right Click
            {
                ray = _Camera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (Debug) UnityEngine.Debug.Log("You right clicked " + hit.collider.gameObject.name, hit.collider.gameObject);

                    hit.transform.gameObject.SendMessage("RightClicked", hit.point, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }
}