﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamera : MonoBehaviour
{
    public enum ControlModeEnum
    {
        None,
        Mouse,
        Keyboard,
        DragHeight,
        DragDeep
    }
    public List<ControlModeEnum> ControlMode = new List<ControlModeEnum>() {
        ControlModeEnum.DragHeight
    };

    public int Boundary = 10;
    public float CameraSpeed = 7;
    public float LimitUpCamera = 400;
    public float LimitRotateUpCamera = 45;
    public float LimitDownCamera = 40;
    public float LimitRotateDownCamera = 15;

    private int theScreenWidth;
    private int theScreenHeight;
    
    void Start()
    {
        theScreenWidth = Screen.width;
        theScreenHeight = Screen.height;
    }
    
    void Update()
    {
        if(ControlMode.Contains(ControlModeEnum.DragHeight) ||
            ControlMode.Contains(ControlModeEnum.DragDeep))
        {
            if (Input.GetMouseButtonDown(0))
            {
                hit_position = Input.mousePosition;
                camera_position = transform.position;

            }
            if (Input.GetMouseButton(0))
            {
                current_position = Input.mousePosition;
                LeftMouseDrag();
            }
        }

        if (ControlMode.Contains(ControlModeEnum.Keyboard))
        {
            if (Input.GetKey(KeyCode.UpArrow))
                transform.position += Vector3.forward * CameraSpeed;

            if (Input.GetKey(KeyCode.RightArrow))
                transform.position += Vector3.right * CameraSpeed;

            if (Input.GetKey(KeyCode.DownArrow))
                transform.position += Vector3.back * CameraSpeed;

            if (Input.GetKey(KeyCode.LeftArrow))
                transform.position += Vector3.left * CameraSpeed;
        }

        if (ControlMode.Contains(ControlModeEnum.Mouse))
        {
            if (Input.mousePosition.y > theScreenHeight - Boundary)
                transform.position += Vector3.forward * CameraSpeed;
            if (Input.mousePosition.x > theScreenWidth - Boundary)
                transform.position += Vector3.right * CameraSpeed;
            if (Input.mousePosition.y < 0 + Boundary)
                transform.position += Vector3.back * CameraSpeed;
            if (Input.mousePosition.x < 0 + Boundary)
                transform.position += Vector3.left * CameraSpeed;
        }

    }

    Vector3 hit_position = Vector3.zero;
    Vector3 current_position = Vector3.zero;
    Vector3 camera_position = Vector3.zero;
    float z = 0.0f;

    void LeftMouseDrag()
    {
        if(ControlMode.Contains(ControlModeEnum.DragHeight) ||
            ControlMode.Contains(ControlModeEnum.DragDeep))
        {
            // From the Unity3D docs: "The z position is in world units from the camera."  In my case I'm using the y-axis as height
            // with my camera facing back down the y-axis.  You can ignore this when the camera is orthograhic.

            if (ControlMode.Contains(ControlModeEnum.DragHeight))
                current_position.z = hit_position.z = camera_position.y;
            else if(ControlMode.Contains(ControlModeEnum.DragDeep))
                current_position.z = hit_position.z = camera_position.y;

            // Get direction of movement.  (Note: Don't normalize, the magnitude of change is going to be Vector3.Distance(current_position-hit_position)
            // anyways.  
            Vector3 direction = Camera.main.ScreenToWorldPoint(current_position) - Camera.main.ScreenToWorldPoint(hit_position);

            // Invert direction to that terrain appears to move with the mouse.
            direction = direction * -1;

            Vector3 position = camera_position + direction;

            transform.position = position;
        }
    }
}