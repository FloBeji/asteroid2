﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CurveAnimationHelper : MonoBehaviour
{
    public float AnimationStart = 0;
    public float DelayBeetweenLoop = 0;
    public AnimationCurve Curve;
    public bool IsLooping;

    private bool _IsActivated;
    private float _StartTime;
    private float _WaitingTime;

    public void Start()
    {
        _IsActivated = false;
        _WaitingTime = AnimationStart;
        StartCoroutine(StartAnimation());
    }

    IEnumerator StartAnimation()
    {
        yield return new WaitForSeconds(_WaitingTime);
        _IsActivated = true;
        _StartTime = Time.fixedTime;
    }

    private void Update()
    {
        if (!_IsActivated)
            return;

        float curveTime = Time.fixedTime - _StartTime;

        if (curveTime > Curve.keys[Curve.keys.Length - 1].time)
        {
            _IsActivated = false;
            _WaitingTime = DelayBeetweenLoop;
            if (IsLooping)
                StartCoroutine(StartAnimation());
            return;
        }

        float curveValue = Curve.Evaluate(curveTime);
        UpdateValue(curveValue);
    }

    public virtual void UpdateValue(float newValue) { }
}
