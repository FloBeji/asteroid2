﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Vector3AnimationHelper : MonoBehaviour
{
    public float AnimationStart = 0;
    public float DelayBeetweenLoop = 0;
    public AnimationCurve X;
    public AnimationCurve Y;
    public AnimationCurve Z;
    public bool IsLooping;

    private bool _IsActivated;
    private bool _IsTerminated;
    private float _StartTime;
    private float _WaitingTime;
    private float _lastX;
    private float _lastY;
    private float _lastZ;

    public void Start()
    {
        _IsActivated = false;
        _WaitingTime = AnimationStart;
        StartCoroutine(StartAnimation());
    }

    public void Stop()
    {
        _IsActivated = false;
        _IsTerminated = true;
    }

    IEnumerator StartAnimation()
    {
        if (!_IsTerminated)
        {
            yield return new WaitForSeconds(_WaitingTime);
            _IsActivated = true;
            _StartTime = Time.fixedTime;
        }
    }

    private void Update()
    {
        if (!_IsActivated)
            return;

        float curveTime = Time.fixedTime - _StartTime;

        if (curveTime > X.keys[X.keys.Length - 1].time)
        {
            _IsActivated = false;
            _WaitingTime = DelayBeetweenLoop;
            if (IsLooping)
                StartCoroutine(StartAnimation());
            return;
        }

        float valueX;
        float valueY;
        float valueZ;

        try { valueX = X.Evaluate(curveTime); }
        catch { valueX = _lastX; }

        try { valueY = Y.Evaluate(curveTime); }
        catch { valueY = _lastY; }

        try { valueZ = Z.Evaluate(curveTime); }
        catch { valueZ = _lastZ; }

        UpdateValue(new Vector3(valueX, valueY, valueZ));
    }

    public abstract void UpdateValue(Vector3 newValue);
}
