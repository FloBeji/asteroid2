﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationOverTime : CurveAnimationHelper {

    public Vector3 Coeff = Vector3.one;
    public Vector3 Base = Vector3.zero;

    public override void UpdateValue(float newValue)
    {
        transform.rotation = Quaternion.Euler(new Vector3(
            Base.x + newValue * Coeff.x,
            Base.y + newValue * Coeff.y,
            Base.z + newValue * Coeff.z));
    }
}
