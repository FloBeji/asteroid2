﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpacityOverTime : CurveAnimationHelper
{
    private Renderer _RenderObject;
    private Renderer _Renderer
    {
        get
        {
            if (_RenderObject == null)
                _RenderObject = GetComponent<Renderer>();
            return _RenderObject;
        }
    }

    public override void UpdateValue(float newValue)
    {
        Color color = _Renderer.material.color;
        _Renderer.material.color = new Color(
            color.r,
            color.g,
            color.b,
            newValue);
    }
}