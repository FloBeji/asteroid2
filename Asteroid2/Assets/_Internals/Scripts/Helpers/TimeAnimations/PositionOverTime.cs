﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionOverTime : Vector3AnimationHelper
{
    public override void UpdateValue(Vector3 newValue)
    {
        transform.position = newValue;
    }
}
