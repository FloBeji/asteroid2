﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {

    public bool OnStart = true;
    public float WaitingTime = 5f;


	void Start () {
        if (OnStart)
            StartWaitAndDestroy();
    }

    public void StartWaitAndDestroy()
    {
        StartCoroutine(WaitAndDestroy());
    }

    IEnumerator WaitAndDestroy()
    {
        yield return new WaitForSeconds(WaitingTime);
        GameObject.Destroy(gameObject);
    }
}
